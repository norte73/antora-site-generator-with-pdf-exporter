'use strict'

const fs = require('fs')
const { Readable: LazyReadable } = require('lazystream')
const { spawn } = require('child_process')

const IS_WIN = process.platform === 'win32'

const shellEscape = IS_WIN
  ? (val) => {
      if (~val.indexOf('"')) {
        const dblQuoteRx = /"/g
        return ~val.indexOf(' ') ? `"${val.replace(dblQuoteRx, '"""')}"` : val.replace(dblQuoteRx, '""')
      } else if (~val.indexOf(' ')) {
        return `"${val}"`
      } else {
        return val
      }
    }
  : (val) => val

async function asciidoctorPdf (source, to = undefined, attributes = {}, opts = {}) {
  const { cmd, ...spawnOpts } = opts
  if (IS_WIN) Object.assign(spawnOpts, { shell: true, windowsHide: true })
  const cmdv = (cmd ? cmd.split(' ') : ['asciidoctor-pdf']).map(shellEscape)
  const args = []
  Object.entries(attributes).reduce((accum, [name, val]) => {
    accum.push('-a', shellEscape(val ? `${name}=${val}` : val === '' ? name : `!${name}${val === false ? '=@' : ''}`))
    return accum
  }, args)
  if (to) args.push('-o', shellEscape(to))
  args.push('-')
  return new Promise((resolve, reject) => {
    const ps = spawn(cmdv[0], [...cmdv.slice(1), ...args], spawnOpts)
    ps.on('close', (code) =>
      code === 0
        ? resolve(to && new LazyReadable(() => fs.createReadStream(to)))
        : reject(new Error(`Command failed: ${ps.spawnargs.join(' ')}`))
    )
    ps.on('error', (err) => reject(err.code === 'ENOENT' ? new Error(`Command not found: ${cmdv.join(' ')}`) : err))
    ps.stdout.on('data', (data) => process.stdout.write(data))
    ps.stderr.on('data', (data) => process.stderr.write(data))
    try {
      ps.stdin.write(source)
    } catch {}
    ps.stdin.end()
  })
}

module.exports = asciidoctorPdf
