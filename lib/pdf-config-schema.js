'use strict'

module.exports = {
  disable: {
    doc: 'A switch that disables the PDF exporter even when the custom generator is in use.',
    format: Boolean,
    default: false,
  },
  publish: {
    doc:
      'A switch that controls whether the exported PDFs are included in the published site. The PDFs are published to the root directory of the corresponding component version. The PDFs are always available under the build folder.',
    format: Boolean,
    default: true,
  },
  build_dir: {
    doc: 'The build directory where the artifacts for the PDF files are assembled and the PDF files are generated.',
    format: String,
    default: './build/pdf',
  },
  clean: {
    doc:
      'A switch that controls whether the build directory is cleaned before use. Inherits its value from the Antora playbook if not set.',
    format: Boolean,
    default: undefined,
  },
  command: {
    doc: 'The command that calls Asciidoctor PDF to convert the compiled AsciiDoc documents to PDFs.',
    format: String,
    default: 'bundle exec asciidoctor-pdf',
  },
  process_limit: {
    doc:
      'The number of concurrent processes the generator should not exceed when converting the compiled AsciiDoc documents to PDF.',
    format: Number,
    default: 1,
  },
  keep_generated_sources: {
    doc:
      'A switch that controls whether the compiled AsciiDoc files are kept in the build directory. This key is useful for debugging.',
    format: Boolean,
    default: false,
  },
  insert_start_page: {
    doc:
      'A switch that controls whether the component version start page is inserted into the top of the PDF if if it is not specified in the navigation file.',
    format: Boolean,
    default: true,
  },
  root_level: {
    doc:
      'The navigation list entry level from which to start making PDFs (0 or 1). 0 makes one PDF per component version. 1 makes a PDF per top-level entry in the navigation for each component version.',
    format: [0, 1],
    default: 1,
  },
  section_merge_strategy: {
    doc:
      'Determines how the generator merges sections from the page content with sections created to represent navigation entries. discrete marks all sections in the content as discrete headings. fuse preserves sections from the page content, inserting sections from a parent entry as the first children in the TOC. enclose acts like fuse, except it encloses sections from a parent entry in a generated section, which then becomes the first child of that parent in the TOC. The title of this section is controlled by the overview-title AsciiDoc attribute.',
    format: ['discrete', 'fuse', 'enclose'],
    default: 'discrete',
  },
  component_versions: {
    doc:
      'A filter that specifies which component versions the generator exports to PDFs. Accepted values can be one or more picomatch patterns. * matches all components and versions. @* matches the latest version of all components. @component-name matches the latest version of the named component. @{component-name,another-component-name} matches the latest version of two different components. *@component-name matches all versions of the named component.',
    format: Array,
    default: ['*'],
  },
  asciidoc: {
    attributes: {
      doc:
        'AsciiDoc document attributes that are applied to each compiled AsciiDoc document when exporting to PDF. These attributes supplement attributes defined in the playbook and component version descriptors. The pdf-theme attribute can be used to specify the theme for Asciidoctor PDF. The attributes can also be used to activate the TOC and specify its depth.',
      format: 'map',
      default: { doctype: 'book' },
    },
  },
}
